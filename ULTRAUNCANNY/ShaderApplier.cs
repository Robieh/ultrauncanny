using System;
using UnityEngine;

namespace ULTRAUNCANNY {
    public class ShaderApplier : MonoBehaviour {
        public Material material;
        private void OnRenderImage(RenderTexture src, RenderTexture dest) {
            Graphics.Blit(src, dest, material);
        }
    }
}