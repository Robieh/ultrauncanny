using HarmonyLib;
using UnityEngine;

namespace ULTRAUNCANNY {
    [HarmonyPatch(typeof(PlatformerMovement), "Start")]
    public class ClashFixer {
        [HarmonyPostfix]
        public static void Postfix(PlatformerMovement __instance) {
           ShaderApplier sa =  __instance.platformerCamera.gameObject.GetComponentInChildren<Camera>().gameObject.AddComponent<ShaderApplier>();
           sa.material = UltraUncanny.Material;
           Debug.Log(":d " + __instance.platformerCamera.gameObject.name);
        }
    }
}