using HarmonyLib;

namespace ULTRAUNCANNY {
    [HarmonyPatch(typeof(CameraController), "CheckMouseReverse")]
    public class MouseFixer {
        [HarmonyPostfix]
        public static void Postfix(CameraController __instance) {
            __instance.reverseX = !__instance.reverseX;
        }
    }
}