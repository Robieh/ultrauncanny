﻿using System.Reflection;
using BepInEx;
using HarmonyLib;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace ULTRAUNCANNY {
    [BepInPlugin(pluginGuid, pluginName, pluginVersion)]
    public class UltraUncanny : BaseUnityPlugin {
        public const string pluginGuid = "robiehh.uk.uncanny";
        public const string pluginName = "ULTRAUNCANNY";
        public const string pluginVersion = "1.3";

        private Shader _shader;
        public static Material Material;

        private bool _warned = false;

        void Awake() {
            var harmony = new Harmony(pluginGuid); // rename "author" and "project"
            harmony.PatchAll();
        }
        
        void Start() {
            AssetBundle bundle = AssetBundle.LoadFromMemory(ULTRAUNCANNY.Properties.Resources.uncanny);
            _shader = bundle.LoadAsset<Shader>("FlipShader");
            Material = new Material(_shader);
            SceneManager.activeSceneChanged += OnSceneChange;
        }
        
        public void OnSceneChange(Scene from, Scene to) {
            GameObject quad = null;
            foreach (Transform t in CameraController.Instance.transform) {
                if (t.name.ToLower().Contains("virtual")) {
                    quad = t.gameObject;
                    break;
                }
            }

            if (quad == null) return;
            ShaderApplier sa = quad.AddComponent<ShaderApplier>();
            sa.material = Material;

            Invoke(nameof(FlipGunsAndStuff), 1f);
            
            // access private field "hudCamera"
            var hudCamera =
                typeof(CameraController).GetField("hudCamera", BindingFlags.NonPublic | BindingFlags.Instance);
            // cast hudCamera to Camera
            var hudCameraObj = hudCamera.GetValue(CameraController.Instance) as Camera;
            hudCameraObj.transform.localScale = new Vector3(-1, 1, 1);
            
            // loop through all gameobjects with Checkpoint component on it
            foreach (var checkpoint in GameObject.FindObjectsOfType<CheckPoint>()) {
                var transform1 = checkpoint.transform;
                Vector3 scale = transform1.localScale;
                scale.x *= -1;
                transform1.localScale = scale;
            }
        }
        
        private void FlipGunsAndStuff() {
            {
                GameObject go = GameObject.FindGameObjectWithTag("GunControl");
                Vector3 scale = go.transform.localScale;
                scale.x *= -1;
                go.transform.localScale = scale;
            }
            {
                Vector3 scale = FistControl.Instance.transform.localScale;
                scale.x *= -1;
                FistControl.Instance.transform.localScale = scale;
            }
            AudioListener audioListener = GameObject.FindObjectOfType<AudioListener>();
            GameObject audioListenerObj = audioListener.gameObject;
            audioListener.enabled = false;
            GameObject dummyAudioListener = new GameObject("AudioListener");
            dummyAudioListener.AddComponent<AudioListener>();
            dummyAudioListener.transform.parent = audioListenerObj.transform;
            dummyAudioListener.transform.rotation = Quaternion.Euler(0, 180, 0);
            dummyAudioListener.transform.localPosition = Vector3.zero;
        }
        
        private void WarnAboutOptions() {
            GameObject hud = new GameObject("hud warn");
            HudMessage hudMessage = hud.AddComponent<HudMessage>();
            hudMessage.message = "MAKE SURE TO SWAP <color=orange>LEFT</color> AND <color=orange>RIGHT</color> MOVEMENT IN OPTIONS";
            hudMessage.timed = true;
            hudMessage.PlayMessage();
        }
    }
}