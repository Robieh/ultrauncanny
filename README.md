# Mirrors all the levels
## Installation
- Install the mod with [BepInEx](https://www.youtube.com/watch?v=meNiXcbPh_s&feature=youtu.be)
  - Mod download [here](https://gitlab.com/Robieh/ultrauncanny/-/releases)
- Switch your Left movement with your Right movement (A to D and D to A)
  - couldn't make it automatic lol, so you move in opposite directions

## Screenshots
![](https://cdn.discordapp.com/attachments/751129736167686334/974994727428563004/unknown.png)
![](https://cdn.discordapp.com/attachments/751129736167686334/974995369773637662/unknown.png)
![](https://cdn.discordapp.com/attachments/751129736167686334/974996320018370630/unknown.png)
